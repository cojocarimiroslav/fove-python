from fove.headset import *



class Management():

    def __init__(self, headset):
        self.headset = headset

    def setup(self):
        self.headset.__init__(ClientCapabilities.Gaze + ClientCapabilities.Orientation + ClientCapabilities.Position)
        self.headset.__enter__()

        print('\n------- Initial checking -------')
        if self.headset.isHardwareConnected():
            print ("Headset is conected")
        else:
            print("Please check conection to the headset")

        if self.headset.isMotionReady():
            print ("Motion is ready")
        else:
            print ("Motion did not started...")

        if self.headset.isEyeTrackingReady():
            print ("Eyetracking is ready")
        else:
            print ("Eyetracking did not started...")

        if self.headset.isPositionReady():
            print ("Position is ready")
        else:
            print ("Position did not started, check if the camera is connected")

        if self.headset.isEyeTrackingCalibrated():
            print("Eye tracking calibrated")
        else:
            self.headset.startEyeTrackingCalibration(restartIfRunning=False)
            while headset.isEyeTrackingCalibrating():
                print("Calibrating")
                time.sleep(1)
            print("Eye tracking calibrated")

        if str(self.headset.checkSoftwareVersions()) == 'ErrorCode.None_':
            print("SW acceptable")
        else:
            print("Update SDK")

        print('------- Checking finished -------\n')

    def kill(self):
        self.headset.__exit__(None, None, None)
        print("\nDevice killed")
