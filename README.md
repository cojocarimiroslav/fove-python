# fove-python

Basic fove interaction in python




Extra utils:
-- Do not forget to add the python/site-packages to your PYTHONPATH;
-- running 64-bit python

Info:

-------------------------------------------------------------
-- headset.getGazeVectors() --

-x- returns a tuple of two - one for each eye, use subsetting to extract info for each - 
(<GazeVector: id: 5334timestamp: 1547294212712000vector: <Vec3: 0.1747, -0.0385563, 0.983867>>, <GazeVector: id: 5334timestamp: 1547294212712000vector: <Vec3: 0.1747, -0.0385563, 0.983867>>)

- to access the id - use headset.getGazeVectors()[0].id - and is returning a 'int'
- to access the timestamp - use headset.getGazeVectors()[0].timestamp - is returning a 'int'; need to be converted?
- to access the vector and the ax - use headset.getGazeVectors()[0].vector.x - is returning a 'float'

--------------------------------------------------------------
-- headset.checkEyesClosed() --

-x- returns a weird object, but we can transform it to string-
Eye.Both

- can be: Eye.Both, Eye.Neither, Eye.Left, Eye.Right

- to access it as a string - use str(headset.checkEyesClosed())

---------------------------------------------------------------
-- headset.getGazeConvergence() --

-x- returns a lot of things!-
<GazeConvergenceData: id: 158636timestamp: 1547297985024000ray: <Ray: <Vec3: 0, 0, 0>, <Vec3: 0, 0, 1>>distance: 0pupilDilation: 1attention: 0>

- to access id - use headset.getGazeConvergence().id
- to access timestamp - use headset.getGazeConvergence().timestamp
- to access ray - use headset.getGazeConvergence().ray
- to access distance - use headset.getGazeConvergence().distance
- to access pupilDilation - use headset.getGazeConvergence().pupilDilation
- to access attention - use headset.getGazeConvergence().attention


----------------------------------------------------------------
