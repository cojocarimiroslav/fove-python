from fove.headset import *
from setup import Management
import time
import sys


class Data():
    
    def __init__(self, headset):
        self.headset = headset

    def basic_eyetracking(self):
        print("\nEye tracking started\n")
        while True:
            try:
                self.headset.getGazeConvergence()
                print("ID:", self.headset.getGazeConvergence().id, 
                " Pupil dilation:", self.headset.getGazeConvergence().pupilDilation, 
                " Attention:", self.headset.getGazeConvergence().attention,
                " Distance:", self.headset.getGazeConvergence().distance,  
                " Ray:",  self.headset.getGazeConvergence().ray, 
                " Timestamp:", self.headset.getGazeConvergence().timestamp)
                print("gaze_left is: ", self.headset.getGazeVectors()[0].vector, 
                "     gaze_right is: ", self.headset.getGazeVectors()[1].vector)
                time.sleep(1)
            except KeyboardInterrupt:
                print("\nExit eye tracking")
                break
