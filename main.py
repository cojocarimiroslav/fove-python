from setup import *
from data import *

headset = Headset(ClientCapabilities.Gaze + ClientCapabilities.Orientation + ClientCapabilities.Position)

device = Management(headset)
data = Data(headset)

device.setup()
data.basic_eyetracking()
device.kill()